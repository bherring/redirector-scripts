require 'net/http'
require 'uri'
require 'optparse'
require 'yaml'
require 'csv'
require 'colorize'

options = {}
o = OptionParser.new do |opts|
	opts.banner = "Usage: redirector_test.rb [options]"

  	opts.on('-c', '--csv CSV', 'CSV File Path') { |v| options[:csv_file] = v }

  	opts.on('-s', '--ssl', 'Include SSL/HTTPS From URLs') do 
		options[:ssl] = true
	end

	opts.on("-h", "--help", "Show this message") do
	    	puts opts
		exit
	end

end

o.parse! 

unless options[:csv_file]
	o.parse! ['-h']
end 


redirections = CSV.read(options[:csv_file])

pass_fail = {
	:pass	=> Array.new,
	:fail	=> Array.new
}

redirections.each_with_index do |row, index|
	next unless index > 0

  	from_url 		= row[1]
  	from_path 		= row[2]
  	from_query 		= row[3]
  	to_url 			= row[4]
  	to_path 		= row[5]
  	to_query 		= row[6]
  	redirection_type 	= row[7]

	next if from_url =~ /^https/ && !options[:ssl]

	from_query_str	= from_query && !from_query.strip.empty? ? "?#{from_query}" : ""
	from_full 	= "#{from_url}#{from_path}#{from_query_str}"

	to_query_str	= to_query && !to_query.strip.empty? ? "?#{to_query}" : ""
	to_full 	= "#{to_url}#{to_path}#{to_query_str}"


	url = URI.parse(from_url)
	req_start_time = Time.now
	res = Net::HTTP.start(url.host, url.port) {|http|
		http.get("#{from_path}?#{from_query}")
	}
	req_stop_time = Time.now

	puts "CSV From URL:\t\t#{from_full}"
	puts "CSV To URL:\t\t#{to_full}"
	puts "Actual Redirect To:\t#{res['location']}"
	puts ""

	if res['location'] == to_full
		pass_fail[:pass] << row
		pass = true
	else
		pass_fail[:fail] << row
		pass = false
	end

	puts "Pass/Fail: 		#{pass ? 'Pass'.green : 'Fail'.red}"
	puts "Request Time (ms): 	#{format("%.2f", (req_stop_time - req_start_time) * 1000.0)}"
	puts "---------------------------------------------------------------------"
	puts ""
	puts ""
end

puts ""
puts ""
puts "Test Results"
puts "------------"
puts "Passed:\t#{pass_fail[:pass].count}".green
puts "Failed:\t#{pass_fail[:fail].count}".red
